package creador;

import com.itextpdf.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.*;

import java.security.*;
import java.security.cert.Certificate;
import java.io.*;

import java.io.*; 

public class CreadorPDF {
	 
	
	// FUENTES (Ver c�mo incluirlas y utilizarlas)
    private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.ITALIC);
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
         
    private static final Font categoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static final Font subcategoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    private static final Font blueFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);    
    private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
     
    private static final String iTextExampleImage = "imagen_share.png";
    
    /**
     * M�todo que genera un PDF concreto a partir de la versi�n iText-5.0.5.
     * @param pdfAGenerar
     */
    public static void generarPDF(File pdfAGenerar) {
        // We create the document and set the file name.        
        // Creamos el documento e indicamos el nombre del fichero.
        try {
            Document document = new Document();
            try {
                PdfWriter.getInstance(document, new FileOutputStream(pdfAGenerar));
            } catch (FileNotFoundException fnfException) {
                System.out.println("[ERROR] No se puede acceder al fichero: " + fnfException);
            }
            document.open();
            
            // A�adimos la meta-informaci�n
            document.addTitle("Generando tabla en PDF");
            document.addSubject("Desde c�digo Java");
            document.addKeywords("PDF, Java, c�digo");
            document.addAuthor("https://gitlab.com/danialcala94");
            document.addCreator("https://gitlab.com/danialcala94");
             
            // Primera p�gina 
            Chunk chunk = new Chunk("Archivo PDF generado desde c�digo en JAVA");
            chunk.setBackground(BaseColor.GRAY);
            
            // Primer cap�tulo
            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
            chapter.setNumberDepth(0);
            chapter.add(new Paragraph("Se ha programado una aplicaci�n que permite, desde c�digo escrito en Java, generar un documento PDF."));
            
            // A�adimos una imagen de portada
            Image image;
            try {
                image = Image.getInstance(iTextExampleImage);  
                image.setAbsolutePosition(2, 150);
                chapter.add(image);
            } catch (BadElementException ex) {
                System.out.println("[ERROR] La imagen no es compatible: " +  ex);
            } catch (IOException ex) {
                System.out.println("[ERROR] Ha habido un problema con la imagen: " +  ex);
            }
            document.add(chapter);
             
            // Segunda p�gina - Algunos elementos
            Chapter chapsSec = new Chapter(new Paragraph("Hola"), 1);
            Chapter chapSecond = new Chapter(new Paragraph(new Anchor("Esto es un ejemplo en el que estamos a�adiendo a�n m�s elementos.")), 1);
            Paragraph paragraphS = new Paragraph("Ha sido desarrollado por https://gitlab.com/danialcala94");
             
            // Underline a paragraph by iText (subrayando un p�rrafo por iText)
            Paragraph paragraphE = new Paragraph("Debajo hay una l�nea de puntos, �verdad? Generada desde c�digo.");
            DottedLineSeparator dottedline = new DottedLineSeparator();
            dottedline.setOffset(-2);
            dottedline.setGap(2f);
            paragraphE.add(dottedline);
            
            // addSection a�ade un elemento numerado (como el �ndice)
            chapSecond.addSection(paragraphE);
             
            Section paragraphMoreS = chapSecond.addSection(paragraphS);
            // List by iText (listas por iText)
            String text = "test 1 2 3 ";
            for (int i = 0; i < 5; i++) {
                text = text + text;
            }
            List list = new List(List.UNORDERED);
            ListItem item = new ListItem(text);
            item.setAlignment(Element.ALIGN_JUSTIFIED);
            list.add(item);
            text = "a b c align ";
            for (int i = 0; i < 5; i++) {
                text = text + text;
            }
            item = new ListItem(text);
            item.setAlignment(Element.ALIGN_JUSTIFIED);
            list.add(item);
            text = "supercalifragilisticexpialidocious ";
            for (int i = 0; i < 3; i++) {
                text = text + text;
            }
            item = new ListItem(text);
            item.setAlignment(Element.ALIGN_JUSTIFIED);
            list.add(item);
            paragraphMoreS.add(list);
            document.add(chapSecond);
             
            // Crear una tabla con PdfPTable
             
            // Usamos varios elementos para a�adir t�tulo y subt�tulo
            Anchor anchor = new Anchor("Tabla");
            anchor.setName("Tablaww");            
            Chapter chapTitle = new Chapter(new Paragraph(anchor), 1);
            Paragraph paragraph = new Paragraph("Parrafito que est� inclu�do en el elemento Tabla.");
            Section paragraphMore = chapTitle.addSection(paragraph);
            paragraphMore.add(new Paragraph("Parrafito que servir�a para introducir la tabla"));
            paragraphMore.add(new Paragraph("\n"));
            
            Integer numColumns = 6;
            Integer numRows = 120;

            // Creamos la tabla
            PdfPTable table = new PdfPTable(numColumns); 
            
            PdfPCell columnHeader;
            // Llenamos las cabeceras de la tabla (primera fila)               
            for (int column = 0; column < numColumns; column++) {
            	columnHeader = null;
            	if (column == 0)
            		columnHeader = new PdfPCell(new Phrase("DNI"));
            	else if (column == 1)
            		columnHeader = new PdfPCell(new Phrase("Nombre"));
            	else if (column == 2)
            		columnHeader = new PdfPCell(new Phrase("Apellidos"));
            	else if (column == 3)
            		columnHeader = new PdfPCell(new Phrase("Salario base"));
            	else if (column == 4)
            		columnHeader = new PdfPCell(new Phrase("Categor�a"));
            	else if (column == 5)
            		columnHeader = new PdfPCell(new Phrase("Antig�edad"));
            	
                columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(columnHeader);
            }
            table.setHeaderRows(1);
            // Llenamos todas las dem�s filas (todas menos la primera)               
            for (int row = 0; row < numRows; row++) {
                for (int column = 0; column < numColumns; column++) {
                	PdfPCell cell = new PdfPCell();
                	if (column == 0)
                		cell = new PdfPCell(new Phrase("75648984L"));
                		//table.addCell("75648984L");
                	else if (column == 1)
                		cell = new PdfPCell(new Phrase("Agapito"));
                		//table.addCell("Agapito");
                	else if (column == 2)
                		cell = new PdfPCell(new Phrase("Garc�a Vera"));
                		//table.addCell("Garc�a Vera");
                	else if (column == 3)
                		cell = new PdfPCell(new Phrase("1200"));
                		//table.addCell("1200");
                	else if (column == 4)
                		cell = new PdfPCell(new Phrase("Camarero"));
                		//table.addCell("Camarero");
                	else if (column == 5)
                		cell = new PdfPCell(new Phrase("2"));
                		//table.addCell("2");
                	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                	table.addCell(cell);
                }
            }
            // We add the table (A�adimos la tabla)
            paragraphMore.add(table);
            // We add the paragraph with the table (A�adimos el elemento con la tabla).
            document.add(chapTitle);
            document.close();
            System.out.println("Se ha generado el archivo PDF.");
        } catch (DocumentException documentException) {
            System.out.println("[ERROR] No se ha podido generar correctamente el documento: " + documentException);
        }
    }
    
    /**
     * Mirar: <a href="https://www.imaginanet.com/blog/firmar-y-verificar-firma-digital-en-pdfs-mediante-java-con-itext.html">enlace</a>
     * @param pdf
     */
    public static void firmarPDF(File pdf) {
    	try {
	    	KeyStore ks = KeyStore.getInstance("pkcs12");
	        ks.load(new FileInputStream("RUTA_CERTIFICADO_PFX"), "CLAVE_PRIVADA_CERTIFICADO".toCharArray());
	        String alias = (String)ks.aliases().nextElement();
	        PrivateKey key = (PrivateKey)ks.getKey(alias, "CLAVE_PRIVADA_CERTIFICADO".toCharArray());
	        Certificate[] chain = ks.getCertificateChain(alias);
	        // Recibimos como par�metro de entrada el nombre del archivo PDF a firmar
	        PdfReader reader = new PdfReader(pdf.getAbsolutePath()); 
	        FileOutputStream fout = new FileOutputStream("RUTA_ARCHIVO_PDF_FIRMADO");
	
	        // A�adimos firma al documento PDF
	        PdfStamper stp = PdfStamper.createSignature(reader, fout, '?');
	        PdfSignatureAppearance sap = stp.getSignatureAppearance();
	        sap.setCrypto(key, chain, null, PdfSignatureAppearance.WINCER_SIGNED);
	        sap.setReason("Firma PKCS12");
	        sap.setLocation("Imaginanet");
	        // A�ade la firma visible. Podemos comentarla para que no sea visible.
	        sap.setVisibleSignature(new Rectangle(100, 100, 200, 200),1,null);
	        stp.close();
	    }
	    catch(Exception e) {
	        e.printStackTrace();
	    }
    }
}
