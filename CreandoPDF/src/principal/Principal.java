package principal;

import java.io.File;

import creador.CreadorPDF;

public class Principal {

    public static void main(String args[]) {
        CreadorPDF.generarPDF(new File("prueba.pdf"));
        CreadorPDF.firmarPDF(new File("prueba.pdf"));
    }

}
